// ==UserScript==
// @name         Rightmove clean shortlist
// @version      0.0.1
// @description  A userscript that removes listings marked as 'Sold STC' from a Rightmove user shortlist
// @author       cudjoelab
// @match        https://www.rightmove.co.uk/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// ==/UserScript==

window.addEventListener('load', function() {
    'use strict';

    let houseStatus = document.getElementsByClassName('status');
    let statusToggle = document.createElement("button");
    let li = document.createElement("li");

    function removeSoldStcItems(ele) {
        if (ele.textContent === "Sold STC") {
            ele.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
        }
    }

    function removeStatus (listingStatus) {
        Array.from(listingStatus).forEach(listing=> {
            removeSoldStcItems(listing);
        })
    }

    function removeNoLongerOnMkt() {
        let marketStatus = document.getElementsByClassName('unpublished-message');
        Array.from(marketStatus).forEach(s=>{
            s.parentNode.parentNode.parentNode.parentNode.parentNode.remove();
        })
    }

    function makeRemoveBtn () {
        var tbs = document.getElementById("mrm-subnavbar-list");
        statusToggle.innerHTML = "Hide sold";
        statusToggle.setAttribute('class', 'esnNIC');
        statusToggle.onclick = () => {
            removeStatus(houseStatus);
            removeNoLongerOnMkt();
        }

        li.setAttribute('class', 'fxkXVw');
        li.setAttribute('class', 'sc-bRBYWo');
        li.setAttribute('class', 'sc-hzDkRC');
        li.setAttribute('id', 'ed-status');
        li.appendChild(statusToggle);

        tbs.appendChild(li);
    }

    makeRemoveBtn();
});
